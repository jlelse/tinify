# Tinify API client for Golang

This is a fork of https://github.com/gwpp/tinify-go with custom fixes

## Fixes

- Small code improvements
- Fix resize option scale

## License

This software is licensed under the MIT License. [View the license](LICENSE).
